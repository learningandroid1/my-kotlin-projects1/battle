import kotlin.random.Random

fun main() {
//    val stackTest = MyStack<Int>()
//
//    val testCollection = mutableListOf<Int>(10, 20, 30, 40)
//
//    for (i in testCollection) {
//        stackTest.push(i)
//    }
//
//    println(stackTest.isEmpty())
//    println(stackTest)
//
//    stackTest.pop()
//    stackTest.pop()
//    println(stackTest)

//    Ammo.STANDARD.makeDamage()
//    Ammo.EXPLOSIVE.makeDamage()
//    Ammo.PIERCING.makeDamage()

    val pistol = Weapons.createPistol()
    val rifle = Weapons.createAssaultRifle()

    println(rifle.magazine)
    pistol.getAmmoToShot()
    rifle.reloadMagazine()
    println(rifle.magazine.size)
    rifle.getAmmoToShot()
    println(rifle.magazine.size)
    rifle.getAmmoToShot()
    rifle.getAmmoToShot()
//    println(pistol.magazine.size)
//    println(pistol.maxAmmo)
//    println(pistol.fireType)
//    println(pistol.magazineIsEmpty)
//    println(pistol.magazine)
//    pistol.getAmmoToShot()[0].makeDamage()
//    pistol.getAmmoToShot()[0].makeDamage()
//    pistol.getAmmoToShot()[0].makeDamage()
//    pistol.getAmmoToShot()[0].makeDamage()
//    pistol.getAmmoToShot()[0].makeDamage()
//    pistol.getAmmoToShot()[0].makeDamage()
//    pistol.getAmmoToShot()[0].makeDamage()
//    pistol.getAmmoToShot()[0].makeDamage()
//    pistol.getAmmoToShot()[0].makeDamage()
//    pistol.getAmmoToShot()[0].makeDamage()
//    pistol.getAmmoToShot()[0].makeDamage() на 11 патроне Na ammo, это окей, ну и соотв, makeDamage() ошибка.
    // но это вроде логично по архитектуре приложения

//    for (ammo in pistol.magazine) {
//        pistol.getAmmoToShot()[ammo].makeDamage() // ammo для выстрела 1, magazine -1.
//        println(pistol.magazine.size)
//    }

//    println("====================")
//
//    val assaultRifle = Weapons.createAssaultRifle()
//    assaultRifle.reloadMagazine()
//    println(assaultRifle.magazine.size)
//    assaultRifle.createAmmo()
//    assaultRifle.getAmmoToShot()[0].makeDamage()
}

fun Int.isSuccess(): Boolean {
    return (1 == Random.nextInt(0, 100 / this))
}

fun Int.isSuccess2(): Boolean {
    return (1 != Random.nextInt(0, this/10 + 1))
}