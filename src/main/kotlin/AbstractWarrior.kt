import kotlin.random.Random

abstract class AbstractWarrior(   // что мне в этом классе сделать абстрактным? и зачем?
    val maxHP: Int,
    override var dodgeChance: Int,
    val accuracy: Int,
    val weapon: AbstractWeapon,
    var currentHP: Int
) : Warrior {

    init {
        weapon.reloadMagazine()
    }

    fun showWarriorInfo() {
        println("${this.javaClass.name}, HP - $currentHP, ammo - ${weapon.magazine.size} / ${weapon.maxAmmo}")
    }

    open fun attack(unit: AbstractWarrior) {  // вот тут, в задании указано, что это метод интерфейса, у меня не оверрайдится. да и зачем - непонятно.
        var totalDamage: Int = 0
        var ammoToShot: List<Ammo>
        try {
            ammoToShot = weapon.getAmmoToShot()
        } catch (e: NoAmmoException) {
            weapon.reloadMagazine()
            println("Skipping turn")
            return
        }

        for (ammo in ammoToShot) { // просто весь блок внутрь try передвинь
                if (!unit.dodgeChance.isSuccess()) {
                    if (accuracy.isSuccess2()) {
                        totalDamage += ammo.makeDamage()
                    } else {
                        println("Miss!")
                    }
                } else {
                    println("Target dodges")
                }
            }
            unit.takeDamage(totalDamage)
    }

    override fun takeDamage(damage: Int) {
        if (currentHP > 0) currentHP -= damage else println("Unit is dead")
        if (currentHP <= 0) isKilled = true
    }
}