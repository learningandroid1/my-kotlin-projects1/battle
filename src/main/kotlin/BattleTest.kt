fun main() {

    //throw NoAmmoException()
    val teamSize1 = usersInput()
    val teamSize2 = usersInput()

    val team1: Team = Team(teamSize1)
    val team2: Team = Team(teamSize2)
    team1.fillTeam()
    team2.fillTeam()
    println("Команда 1 $team1")
    println("Команда 2 $team2")

    val battle = Battle(team1, team2)

    battle.showTeamInfo(team1)
    battle.showTeamInfo(team2)
    battle.battleStatus()

    battle.battleIteration()
}

fun usersInput(): Int{
    println("Введите количество игроков в первой команде: ")
    return readln()?.toIntOrNull() ?: return 0
}