class Battle(val team1: Team, val team2: Team) {

    var battleIsFinished = false

    fun battleStatus() {
        if (!team1.isEmpty() && !team2.isEmpty()) BattleState.BattleInProcess.printStatus()
        else if (!team1.isEmpty() && team2.isEmpty()) BattleState.Team1Win.printStatus()
        else if (!team2.isEmpty() && team1.isEmpty()) BattleState.Team2Win.printStatus()
        else BattleState.Draw.printStatus()
    }

    fun showTeamInfo(team: Team){
        println("В команде ${team.size} бойцов")
        for (unit in team) {
            unit.showWarriorInfo()
        }
    }

    // геттер, проверка списка юнитов team2
    fun battleIteration(){
        while (!team1.isEmpty() || !team2.isEmpty()) {
            val attackerFromTeam1 = team1.random()
            val targetFromTeam2 = team2.random()
            val attackerFromTeam2 = team2.random()
            val targetFromTeam1 = team1.random()

            if (!targetFromTeam2.isKilled && !team2.isEmpty()) {
                println("$attackerFromTeam1 attacks $targetFromTeam2")
                attackerFromTeam1.attack(targetFromTeam2)

                if (targetFromTeam2.isKilled) {
                    println("$targetFromTeam2 is killed")
                    team2.remove(targetFromTeam2)
                }
                if (team2.isEmpty()) {
                    BattleState.Team1Win.printStatus()
                    break
                }
            }

            if (!targetFromTeam1.isKilled && !team1.isEmpty()) {
                println("$attackerFromTeam2 attacks $targetFromTeam1")
                attackerFromTeam2.attack(targetFromTeam1)

                if (targetFromTeam1.isKilled) {
                    println("$targetFromTeam1 is killed")
                    team1.remove(targetFromTeam1)
                }
                if (team1.isEmpty()) {
                    BattleState.Team2Win.printStatus()
                    break
                }
            }
        }
    }
}