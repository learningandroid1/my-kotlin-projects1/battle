interface Warrior {
    var isKilled: Boolean
    var dodgeChance: Int

    fun attack(unit: Warrior)
    fun takeDamage(damage: Int)
}