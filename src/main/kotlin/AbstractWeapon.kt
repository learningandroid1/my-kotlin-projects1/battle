abstract class AbstractWeapon (
    val maxAmmo: Int,
    var fireType: FireType,
    val magazine: MyStack<Ammo> = MyStack(),
    var magazineIsEmpty: Boolean = true
    ){

    abstract fun createAmmo(): Ammo

    open fun reloadMagazine(){
        println("Reloading")
        for (i in magazine.size until maxAmmo){
            magazine.push(createAmmo())
        }
        magazineIsEmpty = false
    }

    open fun getAmmoToShot(): List<Ammo>{
        val shots = mutableListOf<Ammo>()
        val count: Int
        if (magazineIsEmpty){
            println("No ammo")
        }
        if (!magazineIsEmpty && fireType is FireType.SingleShot) {
            shots.add(createAmmo())
            magazine.pop()
        }
        if (!magazineIsEmpty && fireType is FireType.MultiShot){
            count = (fireType as FireType.MultiShot).shots
            if (magazine.size < count) throw NoAmmoException()
            for (ammo in 0 until count) {
                shots.add(createAmmo())
                magazine.pop()
            }
        }
        return shots
    }

}