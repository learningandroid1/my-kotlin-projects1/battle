object Weapons {

    fun createPistol(): AbstractWeapon = object: AbstractWeapon(maxAmmo = 10, fireType = FireType.SingleShot){
        override fun createAmmo(): Ammo = Ammo.STANDARD
    }

    fun createAssaultRifle(): AbstractWeapon = object: AbstractWeapon(maxAmmo = 30, fireType = FireType.MultiShot(7)){
        override fun createAmmo(): Ammo = Ammo.STANDARD
    }

    fun createMachineGun(): AbstractWeapon = object: AbstractWeapon(maxAmmo = 50, fireType = FireType.MultiShot(5)){
        override fun createAmmo(): Ammo = Ammo.STANDARD
    }

    fun createGrenadeLauncher(): AbstractWeapon = object: AbstractWeapon(maxAmmo = 6, fireType = FireType.SingleShot){
        override fun createAmmo(): Ammo = Ammo.EXPLOSIVE
    }

    fun createSniperRifle(): AbstractWeapon = object: AbstractWeapon(maxAmmo = 10, fireType = FireType.SingleShot){
        override fun createAmmo(): Ammo = Ammo.PIERCING
    }
}