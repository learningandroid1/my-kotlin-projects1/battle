
sealed class FireType(open val shots: Int) {
    object SingleShot: FireType(1)

    data class MultiShot(override val shots: Int): FireType(shots)
    //data class QueueShot(val queueSize: Int): FireType(queueSize) не использую.
}