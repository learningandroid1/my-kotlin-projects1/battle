import java.util.Stack

open class MyStack<T> : Stack<T>() {

    //open val stackItems = mutableListOf<T>()

    override fun push(item: T): T {
        return super.push(item)
    }

    override fun pop(): T {
        return super.pop()
    }

    override fun isEmpty(): Boolean {
        return super.isEmpty()
    }
}