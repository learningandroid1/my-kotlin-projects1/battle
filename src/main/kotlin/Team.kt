import java.util.Stack

class Team(val teamSize: Int): MyStack<AbstractWarrior>() {

    fun fillTeam(){
        for (i in 0 until teamSize) {
            if (10.isSuccess()) push(General())
            else if (20.isSuccess()) push(Captain())
            else if (30.isSuccess()) push(Sergeant())
            else push(Soldier())
        }
    }

    fun isKilledAndRemoved(){

    }

}