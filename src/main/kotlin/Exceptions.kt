class NoAmmoException: Exception() {
    override val message = "Not enough ammo"
}