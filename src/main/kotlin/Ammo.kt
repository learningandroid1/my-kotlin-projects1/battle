enum class Ammo(val damage: Int, val critChance: Int, val critK: Int) {
    STANDARD(10, 20, 2),
    EXPLOSIVE(30, 20, 3),
    PIERCING(20, 30, 3);

    fun makeDamage(): Int{
        return if (critChance.isSuccess()) {
            println("Crit! Damage done ${this.damage * critK}")
            this.damage * critK
        } else {
            println("Damage done ${this.damage}")
            this.damage
        }
    }
}