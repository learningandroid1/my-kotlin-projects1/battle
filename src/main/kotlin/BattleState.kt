sealed class BattleState {
    abstract fun printStatus()

    object Draw: BattleState() {
        override fun printStatus() { println("Result is draw") }
    }

    object Team1Win: BattleState() {
        override fun printStatus() { println("Team 1 is winner") }
    }

    object Team2Win: BattleState() {
        override fun printStatus() { println("Team 2 is winner") }
    }

    object BattleInProcess: BattleState(){
        override fun printStatus() {
            println("Battle is in process")
        }
    }

}