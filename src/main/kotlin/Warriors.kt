class Soldier: AbstractWarrior(100, 5, 70, Weapons.createAssaultRifle(), 100) {
    override var isKilled: Boolean = false

    override fun attack(unit: Warrior) {}
}

class Sergeant: AbstractWarrior(120, 10, 80, Weapons.createGrenadeLauncher(), 120) {
    override var isKilled: Boolean = false

    override fun attack(unit: Warrior) {}
}

class Captain: AbstractWarrior(150, 15, 80, Weapons.createMachineGun(), 150) {
    override var isKilled: Boolean = false

    override fun attack(unit: Warrior){}
}

class General: AbstractWarrior(200, 20, 100, Weapons.createSniperRifle(), 200) {
    override var isKilled: Boolean = false

    override fun attack(unit: Warrior){}
}
