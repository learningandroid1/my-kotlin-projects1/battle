fun main() {
    val team1 = Teamm()
    println(team1.team)
    team1.team[0].isKilled = true
    println(team1.teamLive)
}

class Teamm {
    val team = listOf<Warrior>(Soldier(), Sergeant(), Captain())
    val teamLive: List<Warrior>
        get() = team.filter { !it.isKilled }
}